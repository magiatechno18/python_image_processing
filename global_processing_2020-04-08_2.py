import os
from skimage import filters, io
import numpy as np
import cv2
from scipy.signal import find_peaks, peak_prominences
import matplotlib.pyplot as plt
import math
import statistics
from tkinter.filedialog import askdirectory

from scipy import signal
from WriteXls import XlsWrite
from tkinter.filedialog import askdirectory
import peakutils
import math
from peakutils.plot import plot as pplot

import pandas as pd

class gaussian_function:

    def __init__(self, a, xc, sigma, offset):
        self._a = a
        self._xc = xc
        self._sigma = abs(sigma)
        self._offset = offset

    # compute value given gaussian parameters
    def value(self, x):
        return gaussian_function.fun_value(x, self._a, self._xc, self._sigma, self._offset)

    # static function for external computation
    def fun_value(x, a, xc, sigma, offset):
        return a*np.exp(-(x-xc)**2/(2*sigma**2))+offset

    # compute gaussian parameters given input data
    def fit_from_data(x, y):

        from scipy.optimize import curve_fit
        from scipy import asarray as exp

        # compute associated gaussian parameters using non linear optimization
        sum_y = sum(y)
        if sum_y != 0:
            a0 = np.max(y)
            xc0 = sum(x * y) / sum_y
            sigma0 = np.sqrt(sum(y * (x - xc0)**2) / sum_y)
            offset0 = 0
            try:
                popt, pcov = curve_fit(gaussian_function.fun_value, x, y, p0=[a0, xc0, sigma0, offset0])
                res_gaussian = gaussian_function(*popt)
                if res_gaussian._a > 0:
                    return res_gaussian
                else:
                    return gaussian_function(0, x[len(x)//2], 0, 0)
            except:
                return gaussian_function(0, x[len(x)//2], 0, 0)
        else:
            return gaussian_function(0, x[len(x)//2], 0, 0)
    
class magia_processing:
    
    def moving_average(a, n=8) : #8
        ret = np.cumsum(a, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n
    
    def actual_magia_wo_baseline_calcul(self, Yint, band_width):
        mFitGa=self.moving_average(Yint)
        mFitGaMagia=mFitGa-min(mFitGa)
        
        x=np.linspace(0,len(mFitGa),len(mFitGa))
        indexes = peakutils.indexes(mFitGa, thres=0.1, min_dist=band_width*0.75) ##0.
        
        x=np.linspace(0,len(mFitGa),len(mFitGa))
        CorFitGa=np.zeros(len(mFitGa))  
        RawPeak=mFitGa[indexes]
        [RawPeak2,Xpks]=self.reject_outliers(RawPeak, indexes)
        delta=40#40
        if max(Xpks)>0:
            
            for xVal in Xpks:
                #Ycorrected.append(sum(FitGa[xVal-delta:xVal+delta]))
                CorFitGa[xVal-delta:xVal+delta]=1
            #IntCor=sum(Ycorrected)
        #else:
            #IntCor=sum(FitGa/10000)
        ## Correction de FitGa
        if sum(CorFitGa)!=0:
            mFitGaS=CorFitGa*mFitGa/len(Xpks)#♦/sum(CorFitGa)/
            x=np.linspace(0,len(mFitGa),len(mFitGa))
            IntCor=1.66*math.log10(sum(mFitGaS))#*NOrma)   #1.66=norma
            #IntCor=2.5*1.66*math.log10(sum(mFitGa)/NOrma*24e9)-6
            
        else:
            mFitGa=np.nan
            IntCor=0
        return IntCor, mFitGa,indexes
    
        
        
    def actual_magia(self, Yint, band_width):
        # open input image

        Ycrop=Yint[0:len(Yint)]
        
        #calcul de la base line (peakutils)
        base = peakutils.baseline(Ycrop, 17) #17
        Ybsl=(Ycrop-base)#/base
        NOrma=sum(base)#/base.argmax();
        Ybsl=Ybsl-min(Ybsl)
        

        mFitGa=self.moving_average(Ybsl)
        mFitGaMagia=mFitGa-min(mFitGa)
        
        x=np.linspace(0,len(mFitGa),len(mFitGa))
        indexes = peakutils.indexes(mFitGa, thres=0.1, min_dist=band_width*0.75) ##0.
        
        x=np.linspace(0,len(mFitGa),len(mFitGa))
        CorFitGa=np.zeros(len(mFitGa))  
        RawPeak=mFitGa[indexes]
        [RawPeak2,Xpks]=self.reject_outliers(RawPeak, indexes)
        delta=10#40
        if max(Xpks)>0:
            
            for xVal in Xpks:
                #Ycorrected.append(sum(FitGa[xVal-delta:xVal+delta]))
                CorFitGa[xVal-delta:xVal+delta]=1
            #IntCor=sum(Ycorrected)
        #else:
            #IntCor=sum(FitGa/10000)
        ## Correction de FitGa
        if sum(CorFitGa)!=0:
            mFitGaS=CorFitGa*mFitGa/len(Xpks)#♦/sum(CorFitGa)/
            x=np.linspace(0,len(mFitGa),len(mFitGa))
            IntCor=1.66*math.log10(sum(mFitGaS))#*NOrma)   #1.66=norma
            #IntCor=2.5*1.66*math.log10(sum(mFitGa)/NOrma*24e9)-6
            
        else:
            mFitGa=np.nan
            IntCor=0
            
        return IntCor, mFitGa,indexes
    
        
    def moving_average(self,a, n=8) : #8
        ret = np.cumsum(a, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n
    
    def reject_outliers(self,data,Xcoord, m =3.5): #1.5
        s=1
        Xpeaks=[0]
        d = np.abs(data - np.median(data))
        mdev = np.median(d)
        s = d/mdev 
        if len(data)>2:   #3
            h=data[s<m]
            Xpeaks=Xcoord[s<m]
        else:
            h=[0]
            Xpeaks=[0]
        return h, Xpeaks
   
    # main processing method
    def process_image(self, img_path, band_width, angle, max_saturation_x_radius):

        # define processing parameters
        self._img_path = img_path
        self._img_dir_path = os.path.dirname(img_path)
        self._img_file_name = os.path.splitext(os.path.basename(img_path))[0]
        self._img_file_ext = os.path.splitext(img_path)[1]
        self._rotation_angle = angle
        self._skeeped_starting_columns = 200
        self._prefiltering_max_saturation_x_radius = max_saturation_x_radius
        self._prefiltering_dilation_disk_radius = 30
        self._prefiltering_hysteresis_low_thr = 100
        self._max_half_column_span = 20
        self._lambda_sigma = 2
        self._period_range = 10
        self._gaussian_comb = True
        self._with_vertical_length = True

        # open input image
        input_img = self._open_image()

        # smooth input image
        smoothed_input_img = input_img#self._smooth_image(input_img)

        # filter input image
        prefiltering_mask_img = self._prefilter_image(smoothed_input_img)

        # compute background correction
        light_corr_img = self._compute_lightning_correction(smoothed_input_img, band_width)

        # compute associated prefiltered image
        pre_filtered_img = self._apply_inv_mask(light_corr_img, prefiltering_mask_img)

        # compute projection along y
        proj = np.sum(pre_filtered_img, axis=0, dtype=np.double)

        # search for peaks (set self._gaussian_comb = False to switch to Dirac comb)
        peaks = self._find_curve_peaks(proj, band_width)

        # search for peaks width
        gaussian_curves = self._extract_gaussian_curves(proj, peaks)

        # filter gaussian curves
        filtered_gaussian_curves = self._filter_peaks(gaussian_curves)

        # create columns mask around detected peaks
        columns_mask = self._generate_columns_mask(input_img.shape, filtered_gaussian_curves)

        # compute masked input image (prefiltering mask and columns mask)
        filtered_img = self._apply_mask(pre_filtered_img, columns_mask)

        # compute particules mask
        particules_mask_img, vertical_length = self._extract_particules(filtered_img, filtered_gaussian_curves)

        # removal of small particules
        filtered_particules_mask_img = self._remove_small_particules(particules_mask_img, 10)

        # filter image
        final_masked_img = self._apply_mask(filtered_img, filtered_particules_mask_img)

        # compute score
        sum_total = np.sum(final_masked_img)
        sum_mask = np.sum(filtered_particules_mask_img)

        # temporary outputs
        plt.plot(proj)
        plt.plot(np.zeros_like(proj), "--", color="gray")
        for cur_gaussian_curve in filtered_gaussian_curves:
            
            xc = cur_gaussian_curve._xc
            sigma = cur_gaussian_curve._sigma
            offset = cur_gaussian_curve._offset
            a = cur_gaussian_curve._a
            min_x = int(round(max(xc-self._lambda_sigma*sigma, 0)))
            max_x = int(round(min(xc+self._lambda_sigma*sigma, len(proj)-1)))
            x = np.arange(min_x, max_x)
            y = cur_gaussian_curve.value(x)
            plt.vlines(xc - self._lambda_sigma*sigma, 0, offset + a, linestyles='dashed', color='red')
            plt.vlines(xc + self._lambda_sigma*sigma, 0, offset + a, linestyles='dashed', color='red')
            plt.hlines(offset, xc - self._lambda_sigma*sigma, xc + self._lambda_sigma*sigma, linestyles='dashed', color='orange')
            plt.plot(x, y, color="green")

        plt.savefig(os.path.splitext(self._img_path)[0] + "_fig.png", dpi=500)
        plt.close()
        io.imsave(os.path.splitext(self._img_path)[0] + "_input_img.tif", input_img)
        io.imsave(os.path.splitext(self._img_path)[0] + "_prefiltering_mask_img.tif", prefiltering_mask_img)
        io.imsave(os.path.splitext(self._img_path)[0] + "_filtered_particules_mask_img.tif", filtered_particules_mask_img)
        print(self._img_file_name + ";" + str(sum_total) + ";" + str(sum_mask) + ";" + str(len(filtered_gaussian_curves)) + ";" + str(vertical_length))
        return final_masked_img
    # rotate image around its center
    def _rotate_image(self, image, angle):

        image_center = tuple(np.array(image.shape[1::-1]) / 2)
        rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
        result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
        return result

    # open image and prepare it for processing
    def _open_image(self):

        # open input image
        input_img = cv2.imread(self._img_path, cv2.IMREAD_GRAYSCALE)

        # rotate it
        input_rot_img = self._rotate_image(input_img, self._rotation_angle)

        # remove unused area (rotation dead area and first 200 columns)
        input_rot_img = input_rot_img[20:-20, self._skeeped_starting_columns:-20]

        return input_rot_img

    # smoothing pass on image
    def _smooth_image(self, input_img):

        # apply bilateral smoothing
        return cv2.bilateralFilter(input_img, 0, 1.0, 20.0)

    # prefilter image to remove saturation area
    def _prefilter_image(self, input_image):

        # saturation threshold is define as values >250 (we rotate input image
        # and associated values are interpolated)
        saturation_threshold = 250

        # compute saturation mask
        thr, saturation_mask  = cv2.threshold(input_image, saturation_threshold, 1, cv2.THRESH_BINARY)

        # compute erosion of previous mask, we only keep saturations area with large
        # size along x axis
        linear_kernel = np.ones((1, 2*self._prefiltering_max_saturation_x_radius+1), np.uint8)
        saturation_mask_leroded = cv2.erode(saturation_mask, linear_kernel)

        # dilate it around saturations
        disk_diameter = 2*self._prefiltering_dilation_disk_radius+1
        disk_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (disk_diameter, disk_diameter))
        saturation_mask_dilated = cv2.dilate(saturation_mask_leroded, disk_kernel)

        # apply hysteresis around saturations
        hyst_img = filters.apply_hysteresis_threshold(input_image, self._prefiltering_hysteresis_low_thr, saturation_threshold)

        # merge previous results
        masked_hyst_img = saturation_mask_dilated.copy()
        masked_hyst_img[hyst_img==False] = 0

        return masked_hyst_img

    # function allowing to compute image background correction
    def _compute_lightning_correction(self, input_img, band_width):

        # compute lclosing
        kernel1 = np.ones((20, 1),np.uint8)
        tmp_img = cv2.morphologyEx(input_img, cv2.MORPH_OPEN, kernel1)

        # compute lopening
        kernel = np.ones((1, band_width//2),np.uint8)
        bkg_img = cv2.morphologyEx(tmp_img, cv2.MORPH_OPEN, kernel)

        # compute associated normalized lightning correction
        bkg_img[bkg_img==0] = 1
        light_corr_img = input_img.astype(np.double) / bkg_img - 1
        light_corr_img[light_corr_img<0] = 0

        return light_corr_img

    # function allowing to find curve peaks
    def _find_curve_peaks(self, proj, band_width):

        # compute signal minus its mean
        used_proj = proj - np.mean(proj)

        # process all periods
        max_value = -1
        selected_period = -1
        selected_offset = -1
        gaussian_fun = gaussian_function(1, 0, 4, 0)
        for cur_period in range(band_width-self._period_range, band_width+self._period_range):
            
            # create convolution kernel
            nb_intervals = (used_proj.size - 2*self._max_half_column_span) // cur_period
            nb_peaks = nb_intervals + 1
            kernel_size = 2*self._max_half_column_span + nb_intervals * cur_period
            kernel = np.zeros(kernel_size)
            for peak_idx in range(nb_peaks):
                peak_x = self._max_half_column_span + peak_idx*cur_period
                if self._gaussian_comb:
                    for x in range(-self._max_half_column_span, self._max_half_column_span):
                        kernel[x + peak_x] = gaussian_fun.value(x)
                else:
                    kernel[peak_x] = 1
                            
            # compute associated convolution
            conv_values = signal.convolve(used_proj, kernel, mode='same')

            # retrieve maximum value
            max_index = np.argmax(conv_values)
            if max_value < conv_values[max_index]:
                max_value = conv_values[max_index]
                selected_period = cur_period
                selected_offset = max_index % cur_period
        
        # build associated peaks collection
        peaks = []
        peakIndex = selected_offset
        while (peakIndex < used_proj.size - 10):
            peaks.append(peakIndex)
            peakIndex += selected_period
        
        return peaks

    # function allowing to filter a 1d signal using an 'estimator' like processing
    def _filter_1d_signal(self, values):

        # compute median signal value
        median_value = statistics.median(values)

        # compute associate residuals
        residuals = values - median_value
        squared_residuals = np.square(residuals)

        # compute associated score
        cur_score = statistics.median(squared_residuals)

        # compute threshold value
        thr_value = math.sqrt(6.25) * 1.4826 * (1+5.0 / (len(values)-1)) * math.sqrt(cur_score)

        # filter gaussian curves
        filtered_idx = []
        for i in range(len(residuals)):
            if abs(residuals[i]) < thr_value :
                filtered_idx.append(i)

        return filtered_idx

    # function allowing to filter computed peaks
    def _filter_peaks(self, gaussian_curves):

        # filter gaussian curves on peak height
        peak_values = []
        for cur_curve in gaussian_curves:
            peak_values.append(cur_curve._a + cur_curve._offset)
        peak_filtered_idx = self._filter_1d_signal(peak_values)
        peak_filtered_gaussian_curves = np.take(gaussian_curves, peak_filtered_idx)

        # filter gaussian curves on sigma value
        sigma_values = []
        for cur_curve in peak_filtered_gaussian_curves:
            sigma_values.append(cur_curve._sigma)
        sigma_filtered_idx = self._filter_1d_signal(sigma_values)
        sigma_filtered_gaussian_curves = np.take(peak_filtered_gaussian_curves, sigma_filtered_idx)

        return sigma_filtered_gaussian_curves

    # function allowing to analyse 2d peaks and to proceed with a gaussian curve approximation
    def _extract_gaussian_curves(self, proj, peaks):

        gaussian_curves = []
        for cur_peak in peaks:
            min_x = int(round(max(cur_peak-self._max_half_column_span, 0)))
            max_x = int(round(min(cur_peak+self._max_half_column_span, len(proj)-1)))
            x = np.arange(min_x, max_x)
            y = proj[min_x:max_x]
            cur_gaussian_curve = gaussian_function.fit_from_data(x, y)
            gaussian_curves.append(cur_gaussian_curve)

        return gaussian_curves
            
    # function allowing to generate columns mask
    def _generate_columns_mask(self, input_shape, gaussian_curves):
        
        columns_mask = np.zeros(input_shape, np.uint8)
        for cur_gaussian_curve in gaussian_curves:
            xc = cur_gaussian_curve._xc
            sigma = cur_gaussian_curve._sigma
            offset = cur_gaussian_curve._offset
            a = cur_gaussian_curve._a
            min_x = int(round(max(xc-self._lambda_sigma*sigma, 0)))
            max_x = int(round(min(xc+self._lambda_sigma*sigma, input_shape[1]-1)))
            for j in range(min_x, max_x):
                columns_mask[:, j] = 1

        return columns_mask

    # function allowing to extract particules from image
    def _extract_particules(self, filtered_img, filtered_gaussian_curves):

        # convert input image to unsigned char
        min_value = np.min(filtered_img)
        max_value = np.max(filtered_img)
        particules_mask_img = (255 * (filtered_img-min_value) / (max_value-min_value)).astype(np.uint8)
        
        # compute column by column thresholding
        vertical_length = 0
        for cur_gaussian_curve in filtered_gaussian_curves:
            xc = cur_gaussian_curve._xc
            sigma = cur_gaussian_curve._sigma
            if self._lambda_sigma*sigma >= 1:
                min_x = int(round(max(xc-self._lambda_sigma*sigma, 0)))
                max_x = int(round(min(xc+self._lambda_sigma*sigma, filtered_img.shape[1]-1)))
                particules_mask_img[:, min_x:max_x] = cv2.adaptiveThreshold(particules_mask_img[:, min_x:max_x], 255,
                                                                            cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 41, 0)

                # check whether we compute vertical signal length
                if self._with_vertical_length:

                    # disk opening to remove small artifacts
                    disk_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
                    particules_mask_img[:, min_x:max_x] = cv2.morphologyEx(particules_mask_img[:, min_x:max_x], cv2.MORPH_OPEN, disk_kernel)

                    # linear opening to keep only lines with minimum signal width
                    linear_kernel = np.ones((1, 5), np.uint8)
                    particules_mask_img[:, min_x:max_x] = cv2.morphologyEx(particules_mask_img[:, min_x:max_x], cv2.MORPH_OPEN, linear_kernel)

                    # count number of lines with signal
                    vertical_length += np.count_nonzero(np.sum(particules_mask_img[:, min_x:max_x], axis=1))

        return particules_mask_img, vertical_length

    # function allowing to remove small particules from a binary image
    def _remove_small_particules(self, bin_img, min_size):

        # compute connected components
        nb_labels, label_img = cv2.connectedComponents(bin_img, 8, cv2.CV_32S)

        # compute associated histogram to get 'surfaces'
        hist = np.zeros(nb_labels, np.int32)
        for y in range(label_img.shape[0]):
            for x in range(label_img.shape[1]):
                hist[label_img[y, x]] += 1

        # remove small particules from input image
        bin_filtered_img = bin_img.copy()
        for y in range(label_img.shape[0]):
            for x in range(label_img.shape[1]):
                if hist[label_img[y, x]] < min_size:
                    bin_filtered_img[y, x] = 0

        return bin_filtered_img

    # function allowing to mask an input image
    def _apply_mask(self, input_img, mask_img):
        masked_img = input_img.copy()
        masked_img[mask_img==0] = 0

        return masked_img

    # function allowing to mask an input image using an inverted mask
    def _apply_inv_mask(self, input_img, mask_img):
        masked_img = input_img.copy()
        masked_img[mask_img!=0] = 0

        return masked_img

# input script parameters
pathname = askdirectory() # show an "Open" dialog box and return the path to the selected file
pathenameExt=pathname
Excelname=os.path.dirname(pathname)
# process all images in target path
process = magia_processing()

## definition variables stockage
S_Result_ol=[]
S_Result_M=[]
angle = 1
band_width = 100
for i in range(19):
    for j in range(3):
        img_path = pathname+"/"+"XX-%d-%d" % (i, j) + ".png"
        if os.path.exists(img_path):
            Img=process.process_image(img_path, band_width, angle,max_saturation_x_radius = 5)
            print("XX-%d-%d" % (i, j) + ".tiff")
            
            YintOl=Img.sum(axis=0)
            
            [Result_ol,Y_ol,indexes_ol]=process.actual_magia_wo_baseline_calcul(YintOl, band_width)
            S_Result_ol.append(Result_ol)
            
            
            ##### Compa avec MagIA
            input_img = cv2.imread(img_path)
            Yint=input_img.sum(axis=0)
            [Result_M,Y_M,indexes_M]=process.actual_magia(Yint[:,0], band_width)
            S_Result_M.append(Result_M)
            
            ### compa avec Magia + Dirac
            
            
            

            
            
            
            
            ## Plot
            x_ol=np.linspace(0,len(Y_ol),len(Y_ol))
            x_M=np.linspace(0,len(Y_M),len(Y_M))
            
            pplot(x_ol, Y_ol, indexes_ol)
            
    
            pplot(x_M,0.01*Y_M,indexes_M)
            plt.title('Comparatif Magia')
            plt.show()
            
            
            ##########pour 18 puits
            
            
x_scatter=np.linspace(0,len(S_Result_M),len(S_Result_M))
plt.scatter(x_scatter,S_Result_M)
plt.scatter(x_scatter,S_Result_ol)        
plt.show



# Create dataframe
dfR=pd.DataFrame({'Magia':S_Result_M,'Ol':S_Result_ol})
# Create a Pandas Excel writer using XlsxWriter as the engine.
writer = pd.ExcelWriter(pathname+'/CompaMagia.xlsx', engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
dfR.to_excel(writer, sheet_name='Sheet1')

# Close the Pandas Excel writer and output the Excel file.
writer.save()




