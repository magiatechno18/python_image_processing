#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 17:11:29 2020

@author: guillaumeblaire
"""

import os
import numpy as np
import cv2
from scipy.signal import find_peaks
import matplotlib.pyplot as plt

# rotate image around its center
def rotate_image(image, angle):
  image_center = tuple(np.array(image.shape[1::-1]) / 2)
  rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
  result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
  return result

# process to lightning correction
def process_image(img_path, band_width, angle):

    # open input image
    input_img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)

    # rotate it
    input_rot_img = rotate_image(input_img, angle)
    input_rot_img = input_rot_img[10:-10, 10:-10]

    # compute lopening
    kernel = np.ones((1,30),np.uint8)
    bkg_img = cv2.morphologyEx(input_rot_img, cv2.MORPH_OPEN, kernel)

    # compute associated normalized lightning correction
    bkg_img[bkg_img==0] = 1
    light_corr_img = input_rot_img.astype(np.double) / bkg_img - 1

    # compute projection along y
    proj = np.sum(light_corr_img, axis=0, dtype=np.double)
    peaks, dic = find_peaks(proj, distance=0.9*band_width)
    plt.plot(proj)
    plt.plot(peaks, proj[peaks], "x")
    plt.plot(np.zeros_like(proj), "--", color="gray")
    plt.savefig(os.path.splitext(img_path)[0] + "_fig.png")
    plt.close()

# input script parameters
rootPath = "D:\\DataClients\\Magia\\AnalyseImage"
gamme_str = "Gamme-B"
if gamme_str == "GammeA":
    angle = 1
    band_width = 100
    img_file_ext = "tiff"
if gamme_str == "Gamme-B":
    angle = 1
    band_width = 100
    img_file_ext = "tiff"
if gamme_str == "Gamme-C":
    angle = 0
    band_width = 90
    img_file_ext = "png"

# process all images in target path
for i in range(19):
    for j in range(3):
        img_path = os.path.join(rootPath, gamme_str, ("XX-%d-%d." % (i, j)) + img_file_ext)
        if os.path.exists(img_path):
            process_image(img_path, band_width, angle)


