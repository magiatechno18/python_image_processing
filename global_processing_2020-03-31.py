import os
from skimage import filters, io
import numpy as np
import cv2
from scipy.signal import find_peaks, peak_prominences, convolve

import matplotlib.pyplot as plt


from WriteXls import XlsWrite
from tkinter.filedialog import askdirectory
import peakutils
import math
from peakutils.plot import plot as pplot

import pandas as pd

class gaussian_function:

    def __init__(self, a, xc, sigma, offset):
        self._a = a
        self._xc = xc
        self._sigma = sigma
        self._offset = offset

    # compute value given gaussian parameters
    def value(self, x):
        return gaussian_function.fun_value(x, self._a, self._xc, self._sigma, self._offset)

    # static function for external computation
    def fun_value(x, a, xc, sigma, offset):
        return a*np.exp(-(x-xc)**2/(2*sigma**2))+offset

    # compute gaussian parameters given input data
    def fit_from_data(x, y):

        from scipy.optimize import curve_fit
        from scipy import asarray as exp

        # compute associated gaussian parameters using non linear optimization
        a0 = np.max(y)
        xc0 = sum(x * y) / sum(y)
        sigma0 = np.sqrt(sum(y * (x - xc0)**2) / sum(y))
        offset0 = 0
        popt, pcov = curve_fit(gaussian_function.fun_value, x, y, p0=[a0, xc0, sigma0, offset0])

        return gaussian_function(*popt)
    
class magia_processing:
    
    def moving_average(a, n=8) : #8
        ret = np.cumsum(a, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n
    
    def actual_magia_wo_baseline_calcul(self, Yint, band_width):
        mFitGa=self.moving_average(Yint)
        mFitGaMagia=mFitGa-min(mFitGa)
        
        x=np.linspace(0,len(mFitGa),len(mFitGa))
        indexes = peakutils.indexes(mFitGa, thres=0.1, min_dist=band_width*0.75) ##0.
        
        x=np.linspace(0,len(mFitGa),len(mFitGa))
        CorFitGa=np.zeros(len(mFitGa))  
        RawPeak=mFitGa[indexes]
        [RawPeak2,Xpks]=self.reject_outliers(RawPeak, indexes)
        delta=40#40
        if max(Xpks)>0:
            
            for xVal in Xpks:
                #Ycorrected.append(sum(FitGa[xVal-delta:xVal+delta]))
                CorFitGa[xVal-delta:xVal+delta]=1
            #IntCor=sum(Ycorrected)
        #else:
            #IntCor=sum(FitGa/10000)
        ## Correction de FitGa
        if sum(CorFitGa)!=0:
            mFitGaS=CorFitGa*mFitGa/len(Xpks)#♦/sum(CorFitGa)/
            x=np.linspace(0,len(mFitGa),len(mFitGa))
            IntCor=1.66*math.log10(sum(mFitGaS))#*NOrma)   #1.66=norma
            #IntCor=2.5*1.66*math.log10(sum(mFitGa)/NOrma*24e9)-6
            
        else:
            mFitGa=np.nan
            IntCor=0
        return IntCor, mFitGa,indexes
    
        
        
    def actual_magia(self, Yint, band_width):
        # open input image

        Ycrop=Yint[0:len(Yint)]
        
        #calcul de la base line (peakutils)
        base = peakutils.baseline(Ycrop, 17) #17
        Ybsl=(Ycrop-base)#/base
        NOrma=sum(base)#/base.argmax();
        Ybsl=Ybsl-min(Ybsl)
        

        mFitGa=self.moving_average(Ybsl)
        mFitGaMagia=mFitGa-min(mFitGa)
        
        x=np.linspace(0,len(mFitGa),len(mFitGa))
        indexes = peakutils.indexes(mFitGa, thres=0.1, min_dist=band_width*0.75) ##0.
        
        x=np.linspace(0,len(mFitGa),len(mFitGa))
        CorFitGa=np.zeros(len(mFitGa))  
        RawPeak=mFitGa[indexes]
        [RawPeak2,Xpks]=self.reject_outliers(RawPeak, indexes)
        delta=10#40
        if max(Xpks)>0:
            
            for xVal in Xpks:
                #Ycorrected.append(sum(FitGa[xVal-delta:xVal+delta]))
                CorFitGa[xVal-delta:xVal+delta]=1
            #IntCor=sum(Ycorrected)
        #else:
            #IntCor=sum(FitGa/10000)
        ## Correction de FitGa
        if sum(CorFitGa)!=0:
            mFitGaS=CorFitGa*mFitGa/len(Xpks)#♦/sum(CorFitGa)/
            x=np.linspace(0,len(mFitGa),len(mFitGa))
            IntCor=1.66*math.log10(sum(mFitGaS))#*NOrma)   #1.66=norma
            #IntCor=2.5*1.66*math.log10(sum(mFitGa)/NOrma*24e9)-6
            
        else:
            mFitGa=np.nan
            IntCor=0
            
        return IntCor, mFitGa,indexes
    
        
    def moving_average(self,a, n=8) : #8
        ret = np.cumsum(a, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n
    
    def reject_outliers(self,data,Xcoord, m =3.5): #1.5
        s=1
        Xpeaks=[0]
        d = np.abs(data - np.median(data))
        mdev = np.median(d)
        s = d/mdev 
        if len(data)>2:   #3
            h=data[s<m]
            Xpeaks=Xcoord[s<m]
        else:
            h=[0]
            Xpeaks=[0]
        return h, Xpeaks
   
    def diracFindPeaks(self,image, combPeriodRange = range(90,110)):
        
        
        
        # It's far easier for the program to find lines on an image with enhanced
        # contrast. CLAHE seems to be a good candidate for local contrast enhancement.
        clahe = cv2.createCLAHE()
        contrastedImage = clahe.apply(image)
        
        integratedSignal = np.sum(contrastedImage, 0)
        
        maximaTable = []
        shiftTable = []
        
        combOrigin = 0
        toothWidth = 1
        
        for period in combPeriodRange:
            # The comb will have the same size as the original image. It could be
            # larger, but it is not needed as it would just generate duplicate signals.
            comb = np.zeros(integratedSignal.size)
            
            # The maximum number of teeth could be one more, but depending on the
            # size and period, we have a risk of overflow
            maxNumberOfTeeth = int(comb.size / period)
            
            for tooth in range(maxNumberOfTeeth):
                comb[combOrigin + period*tooth : combOrigin + period*tooth + toothWidth] = 1
                
            convolvedSignal = signal.convolve(integratedSignal, comb)
            
            # The maximum of the convolved signal will be reached when the teeth are
            # aligned at best with the luminous zones in the image (i.e., the lines)
            maximumIndex = np.where(convolvedSignal == convolvedSignal.max())[0]
            
            # As we have to choose between several comb periods, we store the maximum
            # we reached. 
            maximaTable.append(convolvedSignal[maximumIndex][0]/maxNumberOfTeeth)
            
            # We also store for which shift (i.e. convolve step) the maximum was
            # reached. It is the distance we moved the comb from its starting position
            # to get the maximum.
            shiftTable.append(maximumIndex)
            
        maximaArray = np.array(maximaTable)
        bestCombIndex = np.where(maximaArray == maximaArray.max())[0][0]
        bestCombPeriod = combPeriodRange[bestCombIndex]
        correspondingShift = shiftTable[bestCombIndex][0]
        
        print("Best comb period : " + str(bestCombPeriod))
        print("Corresponding shift : " + str(correspondingShift))
        
        firstPeakIndex = correspondingShift % bestCombPeriod
        
        peakIndexes = []
        peakIndex = firstPeakIndex# + int(bestCombPeriod / 2)
        
        print (integratedSignal.size)
        while (peakIndex < integratedSignal.size - 10):
            peakIndexes.append(peakIndex)
            peakIndex += bestCombPeriod
        
        return(peakIndexes)    

    # main processing method
    def process_image(self, img_path, band_width, angle):

        # define processing parameters
        self._img_path = img_path
        self._img_dir_path = os.path.dirname(img_path)
        self._img_file_name = os.path.splitext(os.path.basename(img_path))[0]
        self._img_file_ext = os.path.splitext(img_path)[1]
        self._rotation_angle = angle
        self._skeeped_starting_columns = 200
        self._prefiltering_disk_radius = 30
        self._prefiltering_hysteresis_low_thr = 100
        self._max_half_column_span = 20
        self._lambda_sigma = 2

        # open input image
        input_img = self._open_image()

        # filter input image
        prefiltering_mask_img = self._prefilter_image(input_img)

        # compute background correction
        light_corr_img = self._compute_lightning_correction(input_img, band_width)

        # compute associated prefiltered image
        pre_filtered_img = light_corr_img
        pre_filtered_img[prefiltering_mask_img!=0] = 0

        # compute projection along y
        proj = np.sum(light_corr_img, axis=0, dtype=np.double)

        # search for peaks
        peaks = self._find_curve_peaks(proj, band_width)

        # search for peaks width
        
        gaussian_curves = self._extract_gaussian_curves(proj, peaks)

        # create columns mask around detected peaks
        columns_mask = self._generate_columns_mask(input_img.shape, gaussian_curves)

        # compute masked input image (prefiltering mask and columns mask)
        filtered_img = pre_filtered_img
        filtered_img[columns_mask == 0] = 0

        # temporary outputs
#        plt.plot(proj)
#        plt.plot(peaks, proj[peaks], "x")
#        plt.plot(np.zeros_like(proj), "--", color="gray")
        for cur_gaussian_curve in gaussian_curves:
            
            xc = cur_gaussian_curve._xc
            sigma = cur_gaussian_curve._sigma
            offset = cur_gaussian_curve._offset
            a = cur_gaussian_curve._a
            min_x = int(round(max(xc-self._lambda_sigma*sigma, 0)))
            max_x = int(round(min(xc+self._lambda_sigma*sigma, len(proj)-1)))
            x = np.arange(min_x, max_x)
            y = cur_gaussian_curve.value(x)
            plt.vlines(xc - self._lambda_sigma*sigma, 0, offset + a, linestyles='dashed', color='red')
            plt.vlines(xc + self._lambda_sigma*sigma, 0, offset + a, linestyles='dashed', color='red')
            plt.plot(x, y, color="green")
        plt.savefig(os.path.splitext(self._img_path)[0] + "_fig.png", dpi=500)
        plt.close()
        io.imsave(os.path.splitext(self._img_path)[0] + "_filtered_img.tif", filtered_img.astype(np.float32))
        return filtered_img
    # rotate image around its center
    def _rotate_image(self, image, angle):

        image_center = tuple(np.array(image.shape[1::-1]) / 2)
        rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
        result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
        return result

    # open image and prepare it for processing
    def _open_image(self):

        # open input image
        input_img = cv2.imread(self._img_path, cv2.IMREAD_GRAYSCALE)

        # rotate it
        input_rot_img = self._rotate_image(input_img, self._rotation_angle)

        # remove unused area (rotation dead area and first 200 columns)
        input_rot_img = input_rot_img[10:-10, self._skeeped_starting_columns:-10]

        return input_rot_img

    # prefilter image to remove saturation area
    def _prefilter_image(self, input_image):

        # saturation threshold is define as values >250 (we rotate input image
        # and associated values are interpolated)
        saturation_threshold = 250

        # compute saturation mask
        thr, saturation_mask  = cv2.threshold(input_image, saturation_threshold, 1, cv2.THRESH_BINARY)

        # dilate it around saturations
        disk_diameter = 2*self._prefiltering_disk_radius+1
        disk_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (disk_diameter, disk_diameter))
        saturation_mask_dilated = cv2.dilate(saturation_mask, disk_kernel)

        # apply hysteresis around saturations
        hyst_img = filters.apply_hysteresis_threshold(input_image, self._prefiltering_hysteresis_low_thr, saturation_threshold)

        # merge previous results
        masked_hyst_img = saturation_mask_dilated
        masked_hyst_img[hyst_img==False] = 0

        return masked_hyst_img

    # function allowing to compute image background correction
    def _compute_lightning_correction(self, input_img, band_width):

        # compute lopening
        kernel = np.ones((1, band_width//2),np.uint8)
        bkg_img = cv2.morphologyEx(input_img, cv2.MORPH_OPEN, kernel)

        # compute associated normalized lightning correction
        bkg_img[bkg_img==0] = 1
        light_corr_img = input_img.astype(np.double) / bkg_img - 1
        light_corr_img[light_corr_img<0] = 0

        return light_corr_img

    # function allowing to find curve peaks
    def _find_curve_peaks(self, proj, band_width):

        # TODO : integrate dirac comb
        peaks, dic = find_peaks(proj, distance=0.9*band_width)
        
        return peaks

    # function allowing to analyse 2d peaks and to proceed with a gaussian curve approximation
    def _extract_gaussian_curves(self, proj, peaks):

        gaussian_curves = []
        for cur_peak in peaks:
            min_x = int(round(max(cur_peak-self._max_half_column_span, 0)))
            max_x = int(round(min(cur_peak+self._max_half_column_span, len(proj)-1)))
            x = np.arange(min_x, max_x)
            y = proj[min_x:max_x]
            try:
                cur_gaussian_curve = gaussian_function.fit_from_data(x, y)
            except RuntimeError:
                print("Error - curve_fit failed")
                
            #cur_gaussian_curve = gaussian_function.fit_from_data(x, y)
            gaussian_curves.append(cur_gaussian_curve)

        return gaussian_curves
            
    # function allowing to generate columns mask
    def _generate_columns_mask(self, input_shape, gaussian_curves):
        
        columns_mask = np.zeros(input_shape, np.uint8)
        for cur_gaussian_curve in gaussian_curves:
            xc = cur_gaussian_curve._xc
            sigma = 2*cur_gaussian_curve._sigma
            offset = cur_gaussian_curve._offset
            a = cur_gaussian_curve._a
            min_x = int(round(max(xc-self._lambda_sigma*sigma, 0)))
            max_x = int(round(min(xc+self._lambda_sigma*sigma, input_shape[1]-1)))
            for j in range(min_x, max_x):
                columns_mask[:, j] = 1

        return columns_mask

# input script parameters
angle = 1
band_width = 100
pathname = askdirectory() # show an "Open" dialog box and return the path to the selected file
pathenameExt=pathname
Excelname=os.path.dirname(pathname)
# process all images in target path
process = magia_processing()

## definition variables stockage
S_Result_ol=[]
S_Result_M=[]

for i in range(19):
    for j in range(3):
        img_path = pathname+"/"+"XX-%d-%d" % (i, j) + ".tiff"
        if os.path.exists(img_path):
            Img=process.process_image(img_path, band_width, angle)
            
            print("XX-%d-%d" % (i, j) + ".tiff")
            
            YintOl=Img.sum(axis=0)
            
            [Result_ol,Y_ol,indexes_ol]=process.actual_magia_wo_baseline_calcul(YintOl, band_width)
            S_Result_ol.append(Result_ol)
            
            
            ##### Compa avec MagIA
            input_img = cv2.imread(img_path)
            Yint=input_img.sum(axis=0)
            [Result_M,Y_M,indexes_M]=process.actual_magia(Yint[:,0], band_width)
            S_Result_M.append(Result_M)
            
            ### compa avec Magia + Dirac
            
            
            

            
            
            
            
            ## Plot
            x_ol=np.linspace(0,len(Y_ol),len(Y_ol))
            x_M=np.linspace(0,len(Y_M),len(Y_M))
            
            pplot(x_ol, Y_ol, indexes_ol)
            
    
            pplot(x_M,0.01*Y_M,indexes_M)
            plt.title('Comparatif Magia')
            plt.show()
            
            
            ##########pour 18 puits
            
            
x_scatter=np.linspace(0,len(S_Result_M),len(S_Result_M))
plt.scatter(x_scatter,S_Result_M)
plt.scatter(x_scatter,S_Result_ol)        
plt.show



# Create dataframe
dfR=pd.DataFrame({'Magia':S_Result_M,'Ol':S_Result_ol})
# Create a Pandas Excel writer using XlsxWriter as the engine.
writer = pd.ExcelWriter(pathname+'/CompaMagia.xlsx', engine='xlsxwriter')

# Convert the dataframe to an XlsxWriter Excel object.
dfR.to_excel(writer, sheet_name='Sheet1')

# Close the Pandas Excel writer and output the Excel file.
writer.save()



